<?php
/**
 * Created by PhpStorm.
 * User: HUSzanaI
 * Date: 2016. 12. 16.
 * Time: 14:46
 */

Route::group(['prefix' => 'api/v1'], function() {
    Route::resource('comments', '\LaravelIssueTracker\Comments\Controllers\CommentController');
});
