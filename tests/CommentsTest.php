<?php
use LaravelIssueTracker\Core\Tests\Helpers\ApiTester;

class CommentsTest extends ApiTester
{

    use Factory;



    /** @test */
    public function it_fetches_comments()
    {
        $this->times(3)->make('LaravelIssueTracker\Comments\Eloquent\Comment', ['comment' => 'HELLO WORLD']);


        $this->getJson('api/v1/comments');


        $this->assertResponseOk();
    }


    /** @test */
    public function it_fetches_a_signle_comment()
    {
        $this->make('LaravelIssueTracker\Comments\Eloquent\Comment');

        $comment = $this->getJson('api/v1/comments/1')->data;


        $this->assertResponseOk();

        $this->assertObjectHasAttributes($comment, 'comment', 'author');
    }


    /** @test */
    public function it_404s_if_a_comment_is_not_found()
    {
        $this->getJson('api/v1/comments/x');

        $this->assertResponseStatus(404);
    }


    /** @test */
    public function it_create_a_new_comment_give_valid_parameters()
    {
        $this->getJson('api/v1/comments', 'POST', $this->getStub());

        $this->assertResponseStatus(201);
    }


    /** @test */
    public function it_throws_a_422_if_a_new_comment_request_fails_validation()
    {
        $this->getJson('api/v1/comments', 'POST');

        $this->assertResponseStatus(422);
    }

    /**
     * @return array
     */
    protected function getStub()
    {
        return [
            'topic'         => $this->fake->numberBetween(1, 90),
            'author'        => $this->fake->numberBetween(1, 90),
            'type'          => 'comment',
            'comment'       => $this->fake->paragraph(4)
        ];
    }



}