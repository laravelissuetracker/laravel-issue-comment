<?php
namespace LaravelIssueTracker\Comments\Acme\Validators;

use LaravelIssueTracker\Core\Acme\Validators\Validator;


class CommentValidator extends Validator {

    /**
     * @var array
     */
    protected static $rules = [
        'topic'   => 'required',
        'author'  => 'required',
        'type'    => 'required',
        'comment' => 'required'
    ];
}