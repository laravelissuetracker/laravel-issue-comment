<?php
namespace LaravelIssueTracker\Comments\Controllers;

use LaravelIssueTracker\Core\Controller\ApiController;
use Gate;
use Illuminate\Support\Facades\Input;
use LaravelIssueTracker\Comments\Acme\Services\CommentCreatorService;
use LaravelIssueTracker\Comments\Acme\Transformers\CommentTransformer;
use LaravelIssueTracker\Core\Acme\Validators\ValidationException;
use LaravelIssueTracker\Comments\Eloquent\Comment;


class CommentController extends ApiController
{

    /**
     * @var CommentTransformer
     */
    protected $commentTransformer;

    /**
     * @var CommentCreatorService
     */
    protected $commentCreator;

    /**
     * @var int
     */
    protected $limit = 20;

    /**
     * CommentController constructor.
     * @param CommentTransformer $commentTransformer
     * @param CommentCreatorService $commentCreator
     */
    public function __construct(CommentTransformer $commentTransformer, CommentCreatorService $commentCreator)
    {
        $this->commentTransformer = $commentTransformer;
        $this->commentCreator = $commentCreator;
    }


    /**
     * Returns all comments.
     * Results can be ordered by the "created" field which means the date a comment was added.
     *
     *
     * @return JsonResponse JSON object
     * @example GET /api/v1/comments/{issueIdOrKey}
     */
    public function index()
    {
        $comments = Comment::paginate($this->limit);

        return $this->respond([
            'data' => $this->commentTransformer->transformCollection($comments->all())
        ]);

    }

    /**
     * Returns a single comment.
     *
     * @param int $commentId Comment ID
     *
     * @return JsonResponse JSON object
     * @example GET /api/v1/comment/comment/{id}
     */
    public function show($id)
    {
        $comment = Comment::find($id);

        try {
            $this->authorize('show', $comment);
        } catch (\Exception $e) {
            return $this->respondUnathorized($e->getMessage());
        }

        if ( ! $comment )
        {
            return $this->respondNotFound('Comment does not exist');
        }

        return $this->respond([
            'data' => $this->commentTransformer->transform($comment)
        ]);
    }

    /**
     * Adds a new comment.
     *
     * @return JsonResponse JSON object
     * @example POST /api/v1/comment
     */
    public function store()
    {
        try {
            $this->authorize('store');
            $this->commentCreator->make(Input::all());
            return $this->respondCreated('Comment successfully created!');
        }
        catch(ValidationException $e) {
            return $this->respondUnprocessable($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\Request $request, $id)
    {
        try {
            $this->authorize('update', Comment::find($id));
            $this->commentCreator->update(Input::all(), $id);
            return $this->respondCreated('Comment successfully updated!');
        }
        catch(ValidationException $e) {
            return $this->respondUnprocessable($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->commentCreator->destroy($id);
            return $this->respondCreated('Comment successfully destroyed!');
        }
        catch(ValidationException $e) {
            return $this->respondUnprocessable($e->getMessage());
        }
    }

}