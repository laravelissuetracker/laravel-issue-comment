<?php
namespace LaravelIssueTracker\Comments\Acme\Transformers;

use LaravelIssueTracker\Core\Acme\Transformers\Transformer;

class CommentTransformer extends Transformer {

    /**
     * @param $comment
     * @return array
     */
    public function transform($comment)
    {
        return [
            'id'      => $comment['id'],
            'issue'   => $comment['issue_id'],
            'author'  => $comment['author'],
            'comment' => $comment['comment'],
            'parent'  => $comment['parent'],
        ];
    }
}