<?php
namespace LaravelIssueTracker\Comments;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use LaravelIssueTracker\Comments\Eloquent\Comment;
use LaravelIssueTracker\Comments\Listeners\SendDestroyedEmail;
use LaravelIssueTracker\Comments\Policies\CommentPolicy;

class CommentsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $policies = [
        Comment::class => CommentPolicy::class,
    ];

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Event::listen('event.CommentWasDestroyed', function() {
            new SendDestroyedEmail();
        });

        $this->loadRoutesFrom(__DIR__.'/routes.php');

        if( ! $this->app->routesAreCached() ) {
            require __DIR__.'/routes.php';
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}