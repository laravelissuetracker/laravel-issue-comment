<?php

namespace LaravelIssueTracker\Comments\Listeners;

use LaravelIssueTracker\Comments\Events\CommentWasDestroyed;
use Illuminate\Contracts\Mail\Mailer;

class SendDestroyedEmail
{

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        //
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  CommentWasDestroyed  $event
     * @return void
     */
    public function handle(CommentWasDestroyed $event)
    {
        var_dump('sending a destroy email to' . $event->comment->author);
    }
}
