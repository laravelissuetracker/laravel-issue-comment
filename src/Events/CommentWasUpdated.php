<?php
namespace LaravelIssueTracker\Comments\Events;

use Illuminate\Queue\SerializesModels;
use LaravelIssueTracker\Comments\Eloquent\Comment;

class CommentWasUpdated
{
    use SerializesModels;
    /**
     * @var Comment
     */
    private $comment;

    /**
     * Create a new event instance.
     *
     * @param Comment $comment
     */
    public function __construct(Comment $comment)
    {

        $this->comment = $comment;
    }

}
