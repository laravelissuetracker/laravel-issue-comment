<?php
namespace LaravelIssueTracker\Comments\Acme\Services;

use LaravelIssueTracker\Core\Acme\Validators\ValidationException;
use LaravelIssueTracker\Comments\Acme\Validators\CommentValidator;
use LaravelIssueTracker\Comments\Eloquent\Comment;


class CommentCreatorService {

    protected $validator;

    /**
     * CommentCreatorService constructor.
     * @param CommentValidator $validator
     */
    public function __construct(CommentValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param array $attributes
     * @return bool
     * @throws ValidationException
     */
    public function make(array $attributes)
    {
        if( $this->validator->isValid($attributes) )
        {
            $comment = Comment::create($attributes);
            event('CommentWasCreated', $comment);

            return true;
        }

        throw new ValidationException('Comment validation failed', $this->validator->getErrors());
    }

    /**
     * @param array $attributes
     * @return bool
     * @throws ValidationException
     */
    public function update(array $attributes, $id)
    {
        if( $this->validator->isValid($attributes) )
        {
            $comment = Comment::find($id)->update($attributes);
            event('CommentWasUpdated', $comment);

            return true;
        }

        throw new ValidationException('Comment validation failed', $this->validator->getErrors());
    }

    /**
     * @param $id
     * @return bool
     * @throws ValidationException
     */
    public function destroy($id)
    {
        if( Comment::find($id)->exists() )
        {
            $comment = Comment::destroy($id);
            event('CommentWasDestroyed', $comment);

            return true;
        }

        throw new ValidationException('Comment does not exist!', '');
    }
}