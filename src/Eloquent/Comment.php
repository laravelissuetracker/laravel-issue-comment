<?php
namespace LaravelIssueTracker\Comments\Eloquent;

/**
 * Git location: $Id$
 * Package: dao DAO
 *
 * Description: This class for the Comment DAO.
 *
 * Modification History
 * Version  Date          Author                 Description
 * -------  ------------  ---------------------  --------------------------------
 * 1.0      29-AUG-16     Istvan Szana           Created
 */

use Illuminate\Database\Eloquent\Model;


class Comment extends Model
{

    /**
     * Name of the table.
     * @var string
     */
    protected $table = 'comments';

    /**
     * Name if the primary key.
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $hidden = ['deleted_at'];

    /**
     * We want to use these attributes in the methods. These are mandatories.
     * @var array
     */
    protected $fillable = [
        'topic',
        'author',
        'type',
        'parent',
        'comment'
    ];

    public static function boot()
    {
        parent::boot();

        // create a event to happen on updating
        static::updating(function ($table) {
            if ( ! isset( $table->parent ) || $table->parent == '') {
                $table->parent = 0;
            }
        });

        // create a event to happen on saving
        static::saving(function ($table) {
            if ( ! isset( $table->parent ) || $table->parent == '') {
                $table->parent = 0;
            }
        });
    }

}