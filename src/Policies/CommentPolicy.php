<?php

namespace LaravelIssueTracker\Comments\Policies;

use Illuminate\Foundation\Auth\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use LaravelIssueTracker\Comments\Eloquent\Comment;

class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function show(User $user, Comment $comment)
    {
        return true;
    }

    /**
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function store(User $user)
    {
        return true;
    }

    /**
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function update(User $user, Comment $comment)
    {
        return $user->id == $comment->author;
    }

}
